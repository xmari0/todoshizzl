angular.module('todomvc', ['ngRoute', 'ngResource'])
        .config(function ($routeProvider) {
            'use strict';

            var routeConfig = {
                template: 'todoCtrl',
                templateUrl: 'index.html',
                resolve: {
                    store: function (todoStorage) {
                        return todoStorage.then(function (module) {
                            module.get();
                            return module;
                        });
                    }
                }

            };

            $routeProvider
                .when('/', routeConfig)
                .when('/:status', routeConfig)
                .otherwise({
                    redirectTo: '/'
                });
        });

